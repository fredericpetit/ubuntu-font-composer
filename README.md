# Ubuntu font - an automated repository for Composer availability.

_Font is builded in TTF & WOFF2 formats._

| parameter | value |
| ------ | ------ |
| **URL git origin** | [https://git.launchpad.net/ubuntu/+source/fonts-ubuntu/](https://git.launchpad.net/ubuntu/+source/fonts-ubuntu/) 
| **URL git fredericpetit** | [https://gitlab.com/fredericpetit/ubuntu-font-composer/](https://gitlab.com/fredericpetit/ubuntu-font-composer/) |
| **NAMESPACE** (composer) | [ubuntu-font-composer](https://packagist.org/packages/fredericpetit/ubuntu-font-composer/) |
| **RELEASE** (latest) | <span id="tag">2024.03.21</span> |
| **DATE** ('build' commit) | <span id="date">ven. 12 avril 2024 00:49:29 CEST</span> |
| **SCHEDULE** (pipeline) | 11h35 every Monday & Thursday. |

## Usage.
- Add namespace to composer configuration and perform setup.
- Font is in _./assets/font/_.
- Style is in _./assets/css/_.
- Put `<link rel="stylesheet" href="<path>/assets/css/font.min.css">` in HTML.